# Automatic SemVer 2.0 Versioning for .NET Builds

Provides MSBuild targets that hook into the .NET build process to automatically generate a
[SemVer 2.0][semver-2.0]-compatible version based on the current repository revision.

Supports multiple VCS. The default implementation is compatible with common workflows such as Git/Hg Flow or GitHub Flow.
Through several MSBuild injection points the process how version numbers are derived can be influenced to match custom workflows.



# Usage

## Configuration

Set a version prefix in your project file and define the repository type:
```xml
<Project Sdk="Microsoft.NET.Sdk">
  <PropertyGroup>
    <!-- ... -->
    <VersionPrefix>1.2.0</VersionPrefix>
    <RepositoryType>git</RepositoryType>
  </PropertyGroup>
  <!-- ... -->
</Project>
```

The ***version prefix*** is expected to be of format `X.Y.Z`, with a major `X`, minor `Y` and patch version number `Z`.
If the patch or patch and minor version numbers are omitted, they default to `0`.
Additional characters in the version prefix are ignored and will be removed automatically.

The ***repository type*** controls what VCS engine is used to retrieve further information on the repository.
If no repository type is defined, the repository type is auto-detected by searching for a corresponding source control
system metadata-folder in the project root folder.


## Activation

To activate automated versioning, add the following package reference to the project file:
```xml
<Project Sdk="Microsoft.NET.Sdk">
  <ItemGroup>
    <!-- ... -->
    <PackageReference Include="XploRe.Versioning.Tools.DotNet" Version="0.2.0" PrivateAssets="All" />
  </ItemGroup>
</Project>
```

Setting `PrivateAssets` to `All` ensures that the package reference is used for building only and will not appear as a
dependency in the NuSpec file of the project.
If a tool is used to add the dependency, the `PrivateAssets` property should be set automatically.

During each build, the assembly and package version numbers will now automatically include a [SemVer 2.0][semver-2.0]-compatible tag and build number for development and release candidate builds, as well as build metadata.



# Versioning

The versioning tool injects itself into the MSBuild build pipeline and performs the following steps:

```mermaid
flowchart LR

    LoadVcsEngine([Load VCS Engine])
    MSBuildPrev([...])
    MSBuildBuild[Build Target]
    MSBuildPost([...])

    LoadVcsEngine --> MSBuildPrev
    MSBuildPrev ---> VersioningValidateRepositoryType
    VersioningBuildVersion ---> MSBuildBuild
    MSBuildBuild --> MSBuildPost
    
    subgraph Versioning [Versioning Target]
        direction LR

        VersioningValidateRepositoryType[Validate Repository]

        VersioningValidateRepositoryType --> VersioningBuildVersion
    end

    subgraph VersioningBuildVersion [VersioningBuildVersion Target]
        direction TB

        GetVcsProperties(Fetch VCS Repository Properties)
        VersioningPrepareProperties[Prepare Properties]
        VersioningDetermineRelease[VersioningDetermineRelease Target]
        VersioningDetermineReleaseCandiate[VersioningDetermineReleaseCandiate Target]
        VersioningSetBuildNumber[VersioningSetBuildNumber Target]
        VersioningSetPreRelease[VersioningSetPreRelease Target]
        VersioningSetBuild[VersioningSetBuild Target]
        BuildVersion(Build Version)

        GetVcsProperties --> VersioningPrepareProperties
        VersioningPrepareProperties --> VersioningDetermineRelease & VersioningDetermineReleaseCandiate
        VersioningDetermineRelease & VersioningDetermineReleaseCandiate --> VersioningSetVersionTag
        VersioningSetVersionTag --> VersioningSetBuildNumber
        VersioningSetBuildNumber --> VersioningSetPreRelease
        VersioningSetPreRelease --> VersioningSetBuild
        VersioningSetBuild --> BuildVersion
    end

    subgraph VersioningSetVersionTag [VersioningSetVersionTag Target]
        direction LR

        VersionBuildType{Build Type}
        NoTag>"none"]
        ReleaseCandidateTag>"$(VersioningReleaseCandidateTag)"]
        DevelopmentTag>"$(VersioningDevelopmentTag)"]

        VersionBuildType -- release --- NoTag
        VersionBuildType -- release candidate --- ReleaseCandidateTag
        VersionBuildType -- otherwise --- DevelopmentTag
    end
```


## Synthesised Properties
The following properties are automatically synthesised before build or packaging tasks and only if they have not been
defined before:

- `VersionMajor` \
  The major version number, extracted from the `VersionPrefix` property. Defaults to `0` if missing.
- `VersionMinor` \
  The minor version number, extracted from the `VersionPrefix` property. Defaults to `0` if missing.
- `VersionPatch` \
  The patch version number, extracted from the `VersionPrefix` property. Defaults to `0` if missing.
- `VersionCore` \
  The base version number with format `X.Y.Z`.
- `VersionPreRelease` \
  The pre-release identifier part of the final version number (undefined for release builds).
- `VersionBuild` \
  The build metadata part of the final version number.
- `FileVersion` with format `X.Y.P.R` \
  Composed of the major `X`, minor `Y` and patch `P` version number, followed by the repository revision number `R`.
  If the file version is already defined but omits part `R`, the repository revision number is automatically added.
- `AssemblyVersion` with format `X.0.0.0` \
  Composed of the major version number `X` only as [recommended for libraries][dotnet-library-versioning]. 


## Version Format

The final version number has the following format (with SemVer properties shown beneath):
```
VersionPrefix  ["-" VersionTag  ["." BuildNumber] ]  ["+" RevisionID]
<VersionCore>  <--------VersionPreRelease--------->  <-VersionBuild->
```

The version tag, build number or revision ID parts are omitted if they are not defined.

If the version build type indicates
- a release, then version tag and build number are undefined;
- a release candidate, then the version tag defaults to `rc` and the build number is set to the repository revision number;
- a development build, then the version tag defaults to `dev` and the build number is set to the repository revision number.


## Overrides

Control values such as `VersionBuildType` or properties such as `VersionTag` and `BuildNumber` can be overridden by
explicitly setting a value in the project file or via the command-line through the corresponding MSBuild flags:
```sh
dotnet pack /p:VersionTag=beta /p:BuildNumber=2
```

Output:
```
[...]
Building version 1.2.0-beta.2+37d39626d4cb for target netstandard1.3.
[...]
```

An alternative to static overrides are injections into the versioning process.

#### Build Type
The build type controls what parts are included in the version number. See the section on *Version Format* for details.

To manually set the build type, overwrite `VersionBuildType` to:
- `development` for a development build;
- `release candiate` for a release candidate;
- `release` for a release.

This is especially useful in CI systems where the build type is known and can be set explicitly.

## Injections

The versioning pipeline is composed of multiple targets to allow customisation via injection. A custom target can be
configured to run before or after a versioning target to either change prerequisites or postprocess properties. 

### Build Type Detection

To influence the build type detection, the following two targets should be targeted:
- `VersioningDetermineRelease` \
  Sets the `VersionBuildType` property to `release` if the repository indicates a release.
  By default, this is true if either the current repository branch is named `production`, as is typical for continuous
  deployment of internal projects or services, e.g. when using the GitLab Flow workflow; or the current repository tree
  has an associated tag that resembles a version number, as is typical for software that is published under a fixed
  version identifier, and releases are consequently marked with version tags.
- `VersioningDetermineReleaseCandiate` \
  Sets the `VersionBuildType` property to `release-candidate` if the repository indicated a release candidate.
  By default, this is true if either the current branch starts with `release/` as used by many workflows; or the current
  branch is a version branch (i.e. the branch name is a version number with major and minor version).

### Release Tag

Based on the current build tag, the `VersioningSetVersionTag` target derives the `VersionTag` property.
See above diagram for the default implementation.


# Configuration

Options are normal MSBuild properties and can be either defined in the project file or via the command-line.


## Global 

### RepositoryRevisionIdMaxLength `number`
Controls the maximum length of the repository revision ID. If a revision ID is available and longer than the defined
maximum length, the revision ID is truncated:
```sh
dotnet pack /p:VersionTag=beta /p:BuildNumber=2 /p:RepositoryRevisionIdMaxLength=4
```

Output:
```
[...]
Building version 1.2.0-beta.2+37d3 for target netstandard1.3.
[...]
```

The default maximum length is dependent on the VCS engine.

### VersioningDevelopmentTag `string`
The version tag used when a development build is detected. Defaults to `dev`.

### VersioningReleaseCandidateTag `string`
The version tag used when a release candidate build is detected. Defaults to `rc`.

### VersioningReportImportance `high` | `normal` | `low`

Controls versioning status output importance. Defaults to `normal`.
Set to `high` for an overview of version properties during builds.


## Bazaar 

### BazaarToolExe `string`
Path to Bazaar tool executable.
If not provided, the value is auto-determined and defaults to `bzr.exe` on Windows and `bzr` on  other systems,
prepended by `BazaarToolPath` if defined.

Only provide this property if a non-standard name of the Bazaar executable is used.
Executables are resolved against `PATH`. If set, `BazaarToolPath` is ignored and must be prepended manually.

### BazaarToolPath `string`
Path of directory that contains the Bazaar tool executable.
Ignored if `BazaarToolExe` is configured.


## Git

### GitToolExe `string`
Path to Git tool executable.
If not provided, the value is auto-determined and defaults to `git.exe` on Windows and `git` on  other systems,
prepended by `GitToolPath` if defined.

Only provide this property if a non-standard name of the Bazaar executable is used.
Executables are resolved against `PATH`. If set, `GitToolPath` is ignored and must be prepended manually.

### GitToolPath `string`
Path of directory that contains the Bazaar tool executable.
Ignored if `GitToolExe` is configured.


## Mercurial

### MercurialToolExe `string`
Path to Mercurial tool executable.
If not provided, the value is auto-determined and defaults to `hg.exe` on Windows and `hg` on  other systems,
prepended by `MercurialToolPath` if defined.

Only provide this property if a non-standard name of the Mercurial executable is used.
Executables are resolved against `PATH`. If set, `MercurialToolPath` is ignored and must be prepended manually.

### MercurialToolPath `string`
Path of directory that contains the Mercurial tool executable.
Ignored if `MercurialToolExe` is configured.


## Subversion

### SubversionToolExe `string`
Path to Subversion tool executable.
If not provided, the value is auto-determined and defaults to `svn.exe` on Windows and `svn` on  other systems,
prepended by `SubversionToolPath` if defined.

Only provide this property if a non-standard name of the Subversion executable is used.
Executables are resolved against `PATH`. If set, `SubversionToolPath` is ignored and must be prepended manually.

### SubversionToolPath `string`
Path of directory that contains the Subversion tool executable.
Ignored if `SubversionToolExe` is configured.



# License

Released under the [Apache License, Version 2.0][apache-license-2.0].

Copyright © xplo.re IT Services, Michael Maier. \
All rights reserved.


[apache-license-2.0]: <http://www.apache.org/licenses/LICENSE-2.0> "Apache License 2.0"
[semver-2.0]: <https://semver.org/spec/v2.0.0.html> "Semantic Versioning 2.0.0"
[dotnet-library-versioning]: <https://docs.microsoft.com/en-ca/dotnet/standard/library-guidance/versioning#version-numbers> "Versioning and .NET Libraries"
